#!/usr/local/bin/python
import asyncio
import datetime
import sys
import json
import logging
import os
import pytz
import copy
import pprint
from aiohttp import web
import threading
import aiohttp

from abc import ABC, abstractmethod
import redis
import requests
import websockets
from elasticapm.contrib.sanic import ElasticAPM
from sanic import Blueprint, Sanic, response
from sanic.exceptions import SanicException
from sanic.request import Request
from sanic.response import HTTPResponse
from asyncio import CancelledError
from utils import convertTimeStamp, generateTimeStamp

if "test" in sys.argv[0]:
    try:
        import fakeredis
        Redis = fakeredis.FakeStrictRedis
    except ImportError:
        Redis = redis.Redis
else:
    Redis = redis.Redis

def _obfuscate_str(value, pos):
    # return value[:pos]+"*"*(len(value)-pos)
    return value[:pos]+"..."
    
def obfuscate(obj, *keys, pos = 4):
    if type(obj) == dict:
        result = copy.deepcopy(obj)
        for k in keys:
            path = k.split('.')
            size = len(path)
            node = result
            for i, p in enumerate(path):
                # logger.info("OBFUSCATE: {} {} {} {}".format(i,p,type(node),(str(node))))
                if p in node:
                    if i == size - 1:
                        # logger.info("OBFUSCATING {} in path {}, object {}".format(p,path,str(node[p])))
                        #TODO: SHOULD BE CLEANER AND NOT GEM DEPENDENT:
                        x = pos
                        value = str(node[p])
                        if value.startswith('ID:'):
                            continue
                        elif value.startswith('Gem:'):
                            x = 8
                        elif value.startswith('Gebruiker:'):
                            x = 14
                        node[p] = _obfuscate_str(value,x)
                    else:
                        try:
                            data = json.loads(node[p])
                            node[p] = data
                            node = data
                        except Exception as e:
                            node = node[p]
        return result
    else:
        return _obfuscate_str(str(obj),pos) if obj else None        

REDIS_URL = os.getenv("REDIS_URL","redis://redis:6379/8") #TODO: remove default when environment variables are set in deployment
REDIS_PREFIX = os.getenv("REDIS_PREFIX","obi-adapter/")#REDIS_URL = None
# ORG_NAMES = Redis.from_url(url = REDIS_URL, decode_responses=True)

class SessionManager(ABC):
    session_manager = None

    def create_session(self,guid,sender_id, **kwargs):
        logger.info("Create session from SessionManager")
        session = LivechatSession(sender_id=sender_id,guid=guid,session_manager = self, **kwargs)
        # session._load_config()
        # self.update_session(session) DO NOT STORE SESSION! Session only legit after initialization
        return session

    def to_dict(self, guid = None):
        sessions = self.get_sessions()
        now = datetime.datetime.now()
        result = {}
        sessionlist = []
        activelist = []
        queuelist = []
        idlelist = []
        cancelledlist = []
        for sender_id in sessions:
            session = self.get_session(sender_id)
            if guid and session.guid != guid:
                continue
            sdict = session.to_dict()
            task = session.task
            sdict['task'] = str(task)
            sessionlist.append(sdict)
            if session.in_queue:
                queuelist.append(sender_id)
            elif session.cancelled:
                cancelledlist.append(sender_id)
            elif not session.member_removed:
                activelist.append(sender_id)
            else:
                idlelist.append(sender_id)     
        result['stats'] = dict(active=activelist, in_queue=queuelist, idle=idlelist, cancelled=cancelledlist)          
        result['sessions'] = sessionlist
        return result


    @abstractmethod
    def push_callback_msg(self, msg):
        pass

    @abstractmethod
    def pop_callback_msg(self):
        pass

    @abstractmethod
    def get_sessions(self):
        pass

    @abstractmethod
    def get_session(self,sender_id):
        pass

    @abstractmethod
    def update_session(self,session):
        pass

    @abstractmethod
    def delete_session(self,session):
        pass

    @classmethod
    def get_session_manager(cls):
        if not cls.session_manager:
            if not REDIS_URL:
                cls.session_manager = SimpleSessionManager()
            else:
                cls.session_manager = RedisSessionManager()
        return cls.session_manager
        
class SimpleSessionManager(SessionManager):
    def __init__(self):
        self.sessions = {}
        self.callback_messages = []

    def get_session(self,sender_id):
        session = self.sessions.get(sender_id)
        return session

    def get_sessions(self):
        return self.sessions.keys()

    def update_session(self,session):
        self.sessions[session.sender_id] = session
        
    def delete_session(self,session):
        session = self.sessions.pop(session.sender_id, None)

    def push_callback_msg(self,msg):
        self.callback_messages.append(msg)

    def pop_callback_msg(self):
        return self.callback_messages.pop(0)


class RedisSessionManager(SessionManager):
    CALLBACK_QUEUE = "CALLBACK_QUEUE"

    def __init__(self):
        self.sessions = {} #prevent new instances, always check redis for existence
        self.sessiondata = Redis.from_url(url = REDIS_URL, decode_responses=True)

    def get_sessions(self):
        sids = []
        p_sids = self.sessiondata.scan(match=f"{REDIS_PREFIX}*")
        t_sids = [ sid[len(REDIS_PREFIX):] for sid in p_sids[1]]
        sids.extend(t_sids)
        while p_sids[0]>0:
            p_sids = self.sessiondata.scan(p_sids[0],match=f"{REDIS_PREFIX}*")
            t_sids = [ sid[len(REDIS_PREFIX):] for sid in p_sids[1]]
            sids.extend(t_sids)
        if RedisSessionManager.CALLBACK_QUEUE in sids:
            sids.remove(RedisSessionManager.CALLBACK_QUEUE)
        return sids

    def get_session(self,sender_id):
        sessionstr = self.sessiondata.get(f"{REDIS_PREFIX}{sender_id}")
        logger.info(f"requesting {sender_id} with key {REDIS_PREFIX}{sender_id}")
        session = self.sessions.get(sender_id) if sessionstr else None
        if sessionstr and not session: #only when it is available in redis, no autocreate
            logger.info(f"restoring session from redis {sessionstr}")
            data = json.loads(sessionstr)
            guid = data.pop('guid')
            data.pop('sender_id')
            # if 'log' in data:
            #     data.pop('log')
            session = self.create_session(guid,sender_id,**data) 
            self.sessions[sender_id] = session
        else:
            logger.info("reusing session object from memory")

        return session

    def update_session(self,session):
        self.sessions[session.sender_id] = session
        self.sessiondata.set(f"{REDIS_PREFIX}{session.sender_id}", json.dumps(session.to_dict()))

    
    def delete_session(self,session):
        self.sessiondata.delete(f"{REDIS_PREFIX}{session.sender_id}")
        self.sessions.pop(session.sender_id, None)

    def push_callback_msg(self,msg):
        self.sessiondata.lpush(f"{REDIS_PREFIX}{RedisSessionManager.CALLBACK_QUEUE}",json.dumps(msg))

    def pop_callback_msg(self):
        pop = self.sessiondata.rpop(f"{REDIS_PREFIX}{RedisSessionManager.CALLBACK_QUEUE}")
        return json.loads(pop) if pop else None


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

sentry_dsn = os.getenv("OBI_ADAPTER_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk import capture_exception
    from sentry_sdk.integrations.asyncio import AsyncioIntegration
    from sentry_sdk.integrations.sanic import SanicIntegration

    sentry_sdk.init(
        dsn=sentry_dsn,
        integrations=[SanicIntegration(), AsyncioIntegration()],
    )
else:
    capture_exception = lambda *args, **kwargs: None

DEFAULT_REQUEST_TIMEOUT = 10

ROUTER_CALLBACK_URL = "http://router:8000/callback"
ROUTER_API_KEY = os.getenv("ROUTER_API_KEY")
AUTH_HEADERS = {"X-API-Key": ROUTER_API_KEY, "X-API-User-Id": "obi4wan"}

OBI_AVAILABILITY_URL = os.getenv("OBI_AVAILABILITY_URL")

app = Sanic(name="router")
cb = Blueprint("obi_adapter")
configsession = requests.Session()

if os.getenv("ELASTIC_APM_SERVER_URL"):
    environment = os.getenv("ENVIRONMENT")
    app.config.update(
        {
            "ELASTIC_APM_SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL"),
            "ELASTIC_APM_SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN"),
        }
    )
    apm = ElasticAPM(app=app, config={"SERVICE_NAME": f"Gem Obi Adapter {environment}"})


# async def async_request(*args, **kwargs):
#     async with configsession.post(*args, **kwargs) as resp:
#         pass


class LivechatSession:

    def _should_terminate(self):
        # if not self.cancelled:
        #     logger.info("not cancelled so no... waiting for user_disconnect of end session") #TODO: this mostlikely is the problem also cancelled should be included in state
        #     return False
        # if self.cancelled: #terminate immediately so there is no chance for livechat with employee 
        #     logger.info("should terminate: cancelled and in queue or member removed")
        #     return True
        if self.cancelled and (self.in_queue or self.member_removed): #terminate immediately so there is no chance for livechat with employee 
            logger.info("should terminate: cancelled and in queue or member removed")
            return True
        now = datetime.datetime.now()
        if now.day != self.created.day: #fallback to remove all stale connections every day
            logger.info("should terminate: day switch")
            return True
        idleperiod = (now - self.latestactivity).total_seconds()
        # if self.member_removed and idleperiod > 60: #TODO: change idleperiod for production to 3600 (one hour) + member removed vs has member!!!!??
        #     logger.info("idle timeout and actively removed from obi webcare")
        #     return True
        if idleperiod > 3600: #TODO: change idleperiod for production to 3600 (one hour) + member removed vs has member!!!!??
            logger.info("should terminate: idle timeout fallback")
            return True
        return False
        
    async def _manage_obi_connection(self):
        logger.info("starting new task for : {}".format(self.sender_id))
        should_terminate = self._should_terminate()
        attempt = 0
        max_retries = 3
        while not should_terminate and attempt < max_retries:
            pusher_url = "wss://ws-eu.pusher.com/app/8da830a258993eaa5deb?protocol=7&client=js&version=4.3.1&flash=false"
            # if attempt == 0:
            #     pusher_url = "wss://ws-eu.pushers.com/app/8da830a258993eaa5deb?protocol=7&client=js&version=4.3.1&flash=false"
            attempt += 1
            logger.info("establishing connection, attempt {}".format(attempt))
            self.connected = False
            try:
                async with websockets.connect(pusher_url) as websocket:
                    logger.info("OBI connected to: {} from {}".format(websocket.remote_address,websocket.local_address))
                    self.connection = websocket
                    self.connected = await self._handshake(websocket)
                    # self.connected = False
                    if self.connected:
                        attempt = 0
                    else:
                        logger.warning("handshake failed ...")
                        await asyncio.sleep(3)
                        continue
                    #TODO: what now? force deescalate but how, Dylan, Jason...
                    start_time = datetime.datetime.now()
                    timeout = 5
                    autotrigger_config = self.config.get('autoTrigger',{'enabled':False,'triggerOnlyOnce': False})

                    while self.connected and websocket.open and not should_terminate: #TODO: connected usage: if not connected than should deescalate !!! Jason help!
                        try:
                            recv = self.connection.recv()
                            message = await asyncio.wait_for(recv,timeout=timeout)
                            if message:
                                if self.in_queue:
                                    await self.join_agent()
                                await self.on_message(message)
                        except Exception as e:
                            if type(e) == asyncio.exceptions.TimeoutError:
                                # check if queue autotrigger timeouts are expired and messages are needed
                                should_terminate = self._should_terminate()
                                if self.in_queue and autotrigger_config['enabled']:
                                    ctime = datetime.datetime.now()
                                    if not self.autotriggered and (ctime - self.created).total_seconds() > autotrigger_config['delaySeconds']:
                                        await send_router_message(
                                            self.sender_id, text = autotrigger_config['text'],avatar_url=self.get_avatar()
                                        )
                                        self.autotriggered = ctime
                                        self.session_manager.update_session(self)
                                    elif self.autotriggered and not autotrigger_config['triggerOnlyOnce'] and (ctime - self.autotriggered).total_seconds() > autotrigger_config['cooldownTime']*60:                          
                                        await send_router_message(
                                            self.sender_id, text = autotrigger_config['text'],avatar_url=self.get_avatar()
                                        )
                                        self.autotriggered = ctime
                                        self.session_manager.update_session(self)
                            else:
                                logger.error(f"error occured in connectionloop: {str(type(e))}")
                                logger.exception('')

                    # end while loop (finishing connection)    
                    if should_terminate and not self.member_removed: #assume connected = true and websocket.open since should_terminate is the last condition of the while loop
                        logger.info(f"termination initiated, finishing-up OBI connection : {self.sender_id}")
                        text = self._create_timed_context_msg(f"De livechat verbinding met de bezoeker is afgebroken. Je kunt pas weer reageren wanneer de bezoeker contact opneemt.")
                        if self.in_queue:
                            wait = self._get_wait_time()
                            text += f" De bezoeker stond {wait[0]}m {wait[1]}s in de wacht."
                            if wait[0] > 3:
                                logger.error("Queue took longer than 4 minutes", extra=dict(obi_log=self.log,obi_guid=self.guid,obi_id=self.sender_id))
                        await self.sendContextMessage(text)
                    else:
                        logger.info(f"lost connection for session: {self.sender_id}") #TODO: put this in the session storage for debugging???
                    await websocket.close(code=1001)
            except Exception as e0:
                logger.warning(f"exception occured while attempting connection: {str(type(e0))}")
                logger.exception('')
                await asyncio.sleep(3)
            else:
                pass
            self.connected = False
            # logger.info(f"SOMETHING WENT WRONG, LOST... WAITING 20 seconds for debugging:")
            # await asyncio.sleep(20)
        logger.info(f"exiting coroutine for session: {self.sender_id} {self.connection.close_code}")
        self.session_manager.delete_session(self)
        logger.info(f"cleanup tasks...")
        if attempt == max_retries:
            return False
        return True
    
    async def reset(self):
        logger.info("if connection exists, take it down")
        if self.task and not self.task.done():
            self.cancelled = True
            self.member_removed = True
            c = 0
            while self.connected and c < 100: #maximum of 10 seconds for initialization, TODO: parameterize?
                c+=1
                await asyncio.sleep(0.1)
            self.task = None
            self.log = []
            self.cancelled = False
            self.member_removed = False
            self.in_queue = True
            self.created = datetime.datetime.now()
            return not self.connected
        return True
        

    async def up(self):
        # self.latestactivity = datetime.datetime.now() # up does not count as activity itself, the calling method might...
        if not self.config or self._should_terminate():
            logger.info("Session invalid, delete instead of up()")
            self.session_manager.delete_session(self)
            return False

        connection_task_name = self.sender_id
        logger.info("ensure connection...")
        if self.task and self.task.done() and self.task.result():
            self.task = None
        if not self.task: #TODO: what if task.result == false???
            self.connected = False
            logger.info("starting listening for events")
            logger.info("app: {}".format(app))
            # self.session_manager.update_session(self) # should not store session until connection was safely made, no handshake, no session
            loop = app.loop
            self.task = loop.create_task(self._manage_obi_connection(), name=connection_task_name)
            # task = app.add_task(self._manage_obi_connection(), name=connection_task_name) #start coroutine that connects websocket and manages it until daychange (_checkhealth)
            c = 0
            while not self.connected and c < 100: #maximum of 10 seconds for initialization, TODO: parameterize?
                c+=1
                await asyncio.sleep(0.1)
            if self.connected:
                logger.info("created task and connected socket: {}".format(self.task))
                return True
            else:
                logger.error("failed to connect to OBI... status: {}".format(self.to_dict()))    
                #TODO: deescalate right here
                return False
        else:
            logger.info("task available: {}".format(self.task))
            if self.task.done():
                logger.error("task available but ended with no success... status: {}".format(self.to_dict()))
                return False
            return True
        
    async def on_message(self,message):
        decoded = json.loads(message)
        logger.info("Incoming message from agent: %s", obfuscate(decoded))
        recipient_id = decoded["channel"].split("_")[1]
        # config = SESSIONS[recipient_id].config
        event = decoded["event"]
        if event == "client-message":
            self.latestactivity = datetime.datetime.now()
            data = json.loads(decoded["data"])
            message = json.loads(data["message"])
            if message["content"] == "end handover":
                self.cancel()
                logger.info("sending deescalate")
                await send_router_message(recipient_id,custom={"deescalate": True},avatar_url=self.get_avatar())
                text = self._create_timed_context_msg(f"De bezoeker is terug gestuurd naar Gem en kan niet meer reageren tenzij deze zelf weer in de livechat gaat.")
                await self.sendContextMessage(text)
            else:
                self.session_manager.update_session(self)
                await send_router_message(
                    recipient_id,
                    text = message["content"],
                    sender_name=message["author"]["name"],
                    avatar_url=self.get_avatar()
                    # avatar_url=message["author"].get("url",self.get_avatar()) # TODO: GET USER AVATAR, NEEDS GROUP MODIFICATION IN WIDGET
                )
        elif event == "client-typing":
            data = json.loads(decoded["data"])
            router_event = "typing_on" if data["typing"] else "typing_off"
            await send_router_message(recipient_id,custom={"event": router_event},avatar_url=self.get_avatar())
        elif event == "pusher_internal:member_removed":
            self.member_removed = True
            self.session_manager.update_session(self)
        elif event == "pusher_internal:member_added":
            self.member_removed = False
            self.session_manager.update_session(self)

    async def _receive_socket_id(self, websocket):
        logger.info(
            "OBI4wan darth2solo(): Receiving first message from websocket..."
        )  # DEBUG
        connection_resp = await websocket.recv()
        logger.info("WEBSOCKET.RECV() {}".format(connection_resp))
        self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":"wss","type":"recv","message":connection_resp})
        connection_resp_dict = json.loads(connection_resp)
        connection_resp_dict_event = connection_resp_dict["event"]
        connection_event_name = connection_resp_dict_event.split(":")[1]
        logger.info(
            "OBI4wan darth2solo(): Pusher Event: " + connection_event_name
        )  # DEBUG

        if connection_event_name == "connection_established":
            connection_resp_data_dict = json.loads(
                connection_resp_dict["data"]
            )  # DEBUG
            socket_id = connection_resp_data_dict["socket_id"]
            activity_timeout = connection_resp_data_dict["activity_timeout"]
            logger.info("OBI4wan darth2solo(): socket_id: " + socket_id)
            logger.info(
                "OBI4wan darth2solo(): activity_timeout: "
                + str(activity_timeout)
            )
            self.socket_id = socket_id
            return True
        else:
            logger.error("NO CONNECTION ESTABLISHED RECEIVED")
            return False

    async def _authenticate(self, websocket):
            token_req_params = {
                "socket_id": self.socket_id,
                "channel_name": self.channel_name,
                "domain_address": GlobalConfig.domain_address,
                "origin": GlobalConfig.origin,
                "username": self.user_name,
            }
            self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":GlobalConfig.chatapi_uri,"type":"send","message":str(token_req_params)})
            token_req_headers = {"accept": "application/json"}
            logger.info("AUTH TOKEN REQ: {}".format(token_req_params))
            try:
                token_resp = None
                async with aiohttp.ClientSession() as client_session:
                    async with client_session.post(
                        GlobalConfig.chatapi_uri,
                        data=token_req_params,
                        headers=token_req_headers,
                        timeout=DEFAULT_REQUEST_TIMEOUT
                    ) as resp:
                        token_resp = await resp.json()
                        self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":GlobalConfig.chatapi_uri,"type":"recv","message":str(token_resp)})
            except web.HTTPError as e:  # This is the correct syntax
                logger.error("OBI4wan darth2solo(): ChatAPI Error: {}".format(e))
                return False
            else:
                logger.info("AUTH TOKEN RES: {}".format(token_resp))
                logger.info(
                    "OBI4wan darth2solo(): token_resp: " + pprint.pformat(token_resp)
                )  # DEBUG
                auth_token = token_resp["auth"]
                self.auth_token = auth_token
                logger.info(
                    "OBI4wan darth2solo(): self.auth_token: " + auth_token
                )  # DEBUG
                token_channeldata = json.loads(token_resp["channel_data"])
                logger.info(
                    "OBI4wan darth2solo(): token_channeldata: "
                    + pprint.pformat(token_channeldata)
                )  # DEBUG
                user_id = token_channeldata["user_id"]  # "Widget_" + sender_id
                logger.info(
                    "OBI4wan darth2solo(): self.user_id: " + user_id
                )  # DEBUG
                token_channeldata_userinfo = token_channeldata["user_info"]
                token_channeldata_userinfo_name = token_channeldata_userinfo[
                    "name"
                ]  # "widgetUser"
                logger.info(
                    "OBI4wan darth2solo(): token_channeldata_userinfo_name: "
                    + token_channeldata_userinfo_name
                )  # DEBUG
                self.auth_token = auth_token
                self.user_id = user_id
                return True


    async def _subscribe(self, websocket):
        ### Pusher API subscribe
        ## {"event":"pusher:subscribe","data":{"auth":"8da830a258993eaa5deb:96b859be81cd5832cb6f91c21d77e6b22819f1e2c40273ab8f81893bcdaa8796","channel_data":"{\"user_id\":\"Widget_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4\",\"user_info\":{\"name\":\"widgetUser\"}}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_c9190c36-5bd6-4fc9-8b1e-767e87e8b0b4"}}
        ## {"event":"pusher_internal:subscription_succeeded","data":"{\"presence\":{\"count\":1,\"ids\":[\"Widget_dae28d48-9cdf-45ca-a857-313a1be77a1d\"],\"hash\":{\"Widget_dae28d48-9cdf-45ca-a857-313a1be77a1d\":{\"name\":\"widgetUser\"}}}}","channel":"presence-acd3da2b-4906-45da-8373-96e632c899de_dae28d48-9cdf-45ca-a857-313a1be77a1d"}
        logger.info("OBI4wan darth2solo(): Sending event:subscribe...")
        subscribe_channeldata_dict = {
            "user_id": self.user_id,
            "user_info": {"name": self.user_name},
        }
        subscribe_channeldata_json = json.dumps(
            subscribe_channeldata_dict, separators=(",", ":")
        )

        subscribe_dict = {
            "event": "pusher:subscribe",
            "data": {
                "auth": self.auth_token,
                "channel_data": subscribe_channeldata_json,
                "channel": self.channel_name,
            },
        }
        subscribe_json = json.dumps(subscribe_dict, separators=(",", ":"))
        logger.info("WEBSOCKET.SEND() {}".format(subscribe_json))
        self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":"wss","type":"send","message":subscribe_json})
        await websocket.send(subscribe_json)
        logger.info(
            "OBI4wan darth2solo(): Sending event:subscribe (JSON): "
            + subscribe_json
        )  # DEBUG

        # logger.info("OBI4wan darth2solo(): Receiving response for event:subscribe...")
        subscribe_resp = await websocket.recv()
        logger.info("WEBSOCKET.RECV() {}".format(subscribe_resp))
        self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":"wss","type":"recv","message":subscribe_resp})
            # dispatcher.utter_message("Response: {}".format(subscribe_resp)) # DEBUG
        logger.info(
            "OBI4wan darth2solo(): Receiving event:pusher_internal:subscription_succeeded: "
            + subscribe_resp
        )  # DEBUG

        subscribe_resp_dict = json.loads(subscribe_resp)
        subscribe_resp_dict_event = subscribe_resp_dict["event"]
        subscribe_resp_event_name = subscribe_resp_dict_event.split(":")[0]
        logger.info(
            "OBI4wan darth2solo(): subscribe_resp_event_name: {0}".format(
                subscribe_resp_event_name
            )
        )  # DEBUG
        if subscribe_resp_event_name == "pusher_internal":
            subscribe_resp_event_name_sub = subscribe_resp_dict_event.split(
                ":"
            )[1]
            if subscribe_resp_event_name_sub == "subscription_succeeded":
                logger.info(
                    "OBI4wan darth2solo(): subscribe_resp_event_name: {0}:{1}".format(
                        subscribe_resp_event_name, subscribe_resp_event_name_sub
                    )
                )  # DEBUG
                return True
            else:
                logger.info(
                    "OBI4wan darth2solo(): subscribe_resp_event_name: {0}".format(
                        subscribe_resp_event_name
                    )
                )  # DEBUG
                return False
        else:
            return False


    async def _handshake(self, websocket):

        if await self._receive_socket_id(websocket):
            if await self._authenticate(websocket):
                if await self._subscribe(websocket):
                    return True
        return False

    def __init__(self, sender_id:str = None, guid:str = None, session_manager = None, config = None, organisation_name = None, user_name = None, current_page = None, created = None, latestactivity = None, autotriggered = None, member_removed = False, in_queue = True, cancelled = False, initialized = False, log = None):
        if isinstance(latestactivity,float):
            self.latestactivity = datetime.datetime.fromtimestamp(latestactivity)
        elif isinstance(latestactivity,str):
            self.latestactivity = datetime.datetime.fromisoformat(latestactivity)
        else:
            self.latestactivity = datetime.datetime.now()

        if isinstance(created,float):
            self.created = datetime.datetime.fromtimestamp(created)
        elif isinstance(created,str):
            self.created = datetime.datetime.fromisoformat(created)
        else:
            self.created = datetime.datetime.now()

        if isinstance(autotriggered,float):
            self.autotriggered = datetime.datetime.fromtimestamp(autotriggered)
        elif isinstance(autotriggered,str):
            self.autotriggered = datetime.datetime.fromisoformat(autotriggered)
        else:
            self.autotriggered = None

        self.connection = None # cannot be set during construction
        self.config = config #
        self.sender_id = sender_id #
        self.guid = guid #
        self.organisation_name = organisation_name #
        self.user_name = user_name
        self.current_page = current_page #
        self.session_manager = session_manager # not stored but always known runtime by session manager
        self.channel_name = "presence-" + guid + "_" + sender_id #calculate always?
        self.cancelled = cancelled # should not be available, cancelled = deleted... correct behaviour?
        self.in_queue = in_queue #
        self.log = log if log else []
        self.initialized = initialized
        self.member_removed = member_removed #
        self.connected = None # cannot be set during construction
        # self.created = created if created else datetime.datetime.now()
        # self.latestactivity = latestactivity if latestactivity else datetime.datetime.now()
        # self.autotriggered = autotriggered #
        self.task = None

    def to_dict(self):
        return {
            'config': self.config,
            'log': self.log,
            'sender_id': self.sender_id,
            'guid': self.guid,
            'organisation_name': self.organisation_name,
            'current_page': self.current_page,
            'cancelled': self.cancelled,
            'in_queue': self.in_queue,
            'member_removed': self.member_removed,
            'initialized': self.initialized,
            'user_name': self.user_name,
            'created': self.created.isoformat(),
            'latestactivity': self.latestactivity.isoformat(),
            'autotriggered': self.autotriggered.isoformat() if self.autotriggered else None
        }

    def cancel(self): # = schedule cancel not cancelled immediate... healthcheck and loop should do definitive cancelling and deletion
        self.cancelled = True
        self.session_manager.update_session(self)

    def _create_timed_context_msg(self,msg):
        #TODO: use formatting same as page navigation
        now = datetime.datetime.now(pytz.timezone('Europe/Amsterdam'))
        return f"{now:%d-%m-%Y, %H:%M} - {msg}"


    def _get_wait_time(self, start_time = None):
        if not start_time:
            start_time = datetime.datetime.now()
        duration = start_time - self.created
        total_seconds = duration.total_seconds()
        wait_min = int(total_seconds // 60)
        wait_sec = int(total_seconds % 60)
        return (wait_min,wait_sec)
        
    async def join_agent(self):
        self.in_queue = False
        await send_router_message(self.sender_id,custom={"agent_joined": True},avatar_url=self.get_avatar())
        wait = self._get_wait_time()
        text = self._create_timed_context_msg(f"De bezoeker stond {wait[0]}m {wait[1]}s in de wacht")
        if wait[0] > 3:
            logger.error("Queue took longer than 4 minutes", extra=dict(obi_log=self.log,obi_guid=self.guid,obi_id=self.sender_id))
        await self.sendContextMessage(text)
        self.latestactivity = datetime.datetime.now()
        self.session_manager.update_session(self)

    def get_avatar(self):
        if self.config:
            return self.config['widgetStyle'].get('logoUri',None)
        return None
    
    async def init(self,organisation_name, history, page_url):
        self.config = configsession.get(GlobalConfig.config_uri.format(self.guid)).json() #TODO: could be better error handling
        chatFormKeys =  self.config.get("preChatFormKeys",[])
        self.organisation_name = organisation_name
        self.user_name = f"Gem livechat {organisation_name}"
        await self.reset() # Reset connection and variables if already exists
        self.log = []
        self.cancelled = False
        if chatFormKeys:
            if chatFormKeys[0] == 'guid':
                self.user_name = self.sender_id.replace('-',' ')
            if chatFormKeys[0] == 'title':
                self.user_name = self.config.get("widgetStyle",{}).get("title",self.user_name)
        # self.clientname = f"Gem livechat {organisation_name}"
        self.current_page = page_url
        logger.info("Loaded config: {}".format(self.config))
        if await self.up():
            await self.sendContextMessage("ID: {}".format(self.sender_id))
            await asyncio.sleep(0.2)
            await self.sendPageMessage(page_url)
            await asyncio.sleep(0.2)
            await self.sendContextMessage(history)
            #TODO: hardcoded welcome message. fixme
            if not self.config.get("widgetStyle",{}).get("welcomeLine"):
                self.config["widgetStyle"]["welcomeLine"] = "Een moment geduld."

            welcome_message = self.config["widgetStyle"]["welcomeLine"]
            logger.info("Sending welcome message: %s", welcome_message)
            await send_router_message(
                self.sender_id, text = welcome_message, avatar_url=self.get_avatar()
            )
            self.initialized = True
            self.session_manager.update_session(self)
            return True
        else:
            logger.info("Failed to create session...")
            return False


    async def _send(self, msg):
        if await self.up():
            self.latestactivity = datetime.datetime.now()
            self.session_manager.update_session(self)
            logger.info("sending message: {} to address: {} from address: {}".format(obfuscate(json.loads(msg),'data.message.content'),self.connection.remote_address, self.connection.local_address))
            if self.in_queue:
                self.log.append({"timestamp":datetime.datetime.now().isoformat(),"endpoint":"wss","type":"send","message":obfuscate(json.loads(msg),'data.message.content')})
            await self.connection.send(msg)

    async def sendMessage(self, message):
        await self._send(
                self.generateMessageJSON(
                message, datetime.datetime.now().timestamp()
            ))


    async def sendContextMessage(self, message):
        await self._send(
            self.generateContextMessageJSON(
                message, datetime.datetime.now().timestamp()
            )
        )

    async def sendPageMessage(self, page_url):
        await self._send(
            self.generatePageJSON(
                page_url, datetime.datetime.now().timestamp()
            )
        )

    def generatePageJSON(self, page_url, timestamp):
        pagehistory_datetime = convertTimeStamp(timestamp)
        message_data_message_dict = [{
            "date": pagehistory_datetime,
            "url": page_url,
        }]
        message_data_message_json = json.dumps(
            message_data_message_dict, separators=(",", ":")
        )
        message_dict = {
            "event": "client-page-history",
            "data": {"clientPageHistory": message_data_message_json},
            "channel": self.channel_name,
        }
        message_json = json.dumps(message_dict, separators=(",", ":"))
        return message_json


    def generateMessageJSON(self, message, timestamp):
        message_datetime = convertTimeStamp(timestamp)
        message_data_message_author_dict = {"url": None, "name": self.user_name}
        message_data_message_dict = {
            "date": message_datetime,
            "content": message,
            "type": "incoming",
            "author": message_data_message_author_dict,
            "attachment": None,
            "displayInWidget": True,
            "eventType": "client-message",
            "url": GlobalConfig.domain_address,
        }
        message_data_message_json = json.dumps(
            message_data_message_dict, separators=(",", ":")
        )
        message_dict = {
            "event": "client-message",
            "data": {"message": message_data_message_json},
            "channel": self.channel_name,
        }
        message_json = json.dumps(message_dict, separators=(",", ":"))
        return message_json

    def generateContextMessageJSON(self, message, timestamp):
        if timestamp == None:
            contextmessage_datetime = generateTimeStamp()
        else:
            contextmessage_datetime = convertTimeStamp(timestamp)
        contextmessage_data_message_author_dict = {
            "url": None,
            "name": self.user_name,
        }
        contextmessage_data_message_dict = {
            "date": contextmessage_datetime,
            "content": message,
            "type": "incoming",
            "author": contextmessage_data_message_author_dict,
            "attachment": None,
            "displayInWidget": False,
            "eventType": "client-context-message",
            "url": None,
        }
        contextmessage_data_message_json = json.dumps(
            contextmessage_data_message_dict, separators=(",", ":")
        )
        contextmessage_dict = {
            "event": "client-context-message",
            "data": {"message": contextmessage_data_message_json},
            "channel": self.channel_name,
        }
        contextmessage_json = json.dumps(contextmessage_dict, separators=(",", ":"))
        return contextmessage_json


SESSIONS = {}
CURRENT_PAGES = {}
TASKS = {}


class GlobalConfig:
    # sender_id = ""
    # socket_id = ""
    # channel_name = ""
    # activity_timeout = 0  # default value
    appid = ""  # "8da830a258993eaa5deb" # unique for each municipality
    guid = "acd3da2b-4906-45da-8373-96e632c899de"  # unique for each municipality
    # ws_uri = "ws://ws-eu.pusher.com/app/{0}?protocol=7&client=js&version=4.3.1&flash=false:8765".format(appid)
    ws_uri = "ws://ws-eu.pusher.com/app/{0}?protocol=7&client=js&version=4.3.1&flash=false:8765"
    # availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}".format(guid)
    availability_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/availability/{0}"
    chatapi_uri = "https://chatapi.obi4wan.com/api/v1.0/token"
    config_uri = "https://cloudstatic.obi4wan.com/api/v1.0/chat/configuration/{0}"
    domain_address = ""  #'http://localhost/'
    # websocket = None
    auth_token = ""
    user_id = ""
    # clientname = "ChatBotGem"
    origin = "widget"  # can we change this?
    # user_name = "widgetUser"
    time_machine = None

    # sender_id = "foo"
    caption_chathistory = "Chat History from ChatBotGem 🦄"
    caption_chatstarted = "Chat started 🦄"
    caption_chatlinkshared = "Link to Livechat shared with user 🦄"


# def log_exceptions(fn): THIS IS A METHOD DESCRIPTOR, WHAT DOES IT DO?
#     async def run_with_log(*args, **kwds):
#         try:
#             return await fn(*args, **kwds)
#         except Exception as e:
#             logger.exception("Something went wrong while establishing OBI connection")
#             capture_exception(e)
#             raise

#     return run_with_log


async def send_callback_messages():
    logger.info("starting callback listener")
    ct = asyncio.current_task()
    while not ct.cancelled():
        try:
            session_manager = SessionManager.get_session_manager()
            token_resp = None
            async with aiohttp.ClientSession() as client_session:
                logger.info("created callback client session")
                while True:
                    msg = session_manager.pop_callback_msg()
                    if msg:
                        logger.info("sending message: {} {}".format(obfuscate(msg,'text'), type(msg)))
                        async with client_session.post(
                            ROUTER_CALLBACK_URL,
                            json=msg,
                            headers=AUTH_HEADERS,
                            timeout=DEFAULT_REQUEST_TIMEOUT
                        ) as resp:
                            logger.info("http status: {}".format(resp.status))
                    else:
                        await asyncio.sleep(0.5)
        except CancelledError:
            logger.info("catched CancelledError")
            stop = True
        except GeneratorExit:
            logger.info("catched GeneratorExit")
            stop = True
        except Exception as e:  # This is the correct syntax
            logger.error("Error while sending callback messages: {}".format(e))

async def get_status_from_obi(guid):
    try:
        availability_resp = None
        async with aiohttp.ClientSession() as client_session:
            async with client_session.get(
                GlobalConfig.availability_uri.format(guid),
                headers={"accept": "application/json"},
                timeout=DEFAULT_REQUEST_TIMEOUT,
            ) as resp:
                availability_resp = await resp.json()
    except web.HTTPError as e:
        logger.error("Error occurred while requesting availability API: %s", e)
        return {"status": "not_ok"}
    else:
        availability = availability_resp["available"]
        if availability:
            logger.info("OBI4wan: Agent available")  # DEBUG
            return {"status": "ok"}
        else:
            if availability_resp.get("unavailableReason") == "NoAgentsOnline":
                logger.info("OBI4wan: Agent NOT available, no agents online")  # DEBUG
                return {"status": "not_available"}
            elif availability_resp.get("unavailableReason") == "OutsideOpeningHours":
                logger.info("OBI4wan: outside opening hours")  # DEBUG
                return {"status": "closed"}
            else:
                logger.info("OBI4wan: not available for unknown reason")  # DEBUG
                return {"status": "not_ok"}



@cb.route("/<guid>/webhook", methods=["GET", "POST"])
async def webhook(request, guid):
    if request.method == "GET":
        result = await get_status_from_obi(guid) | SessionManager.get_session_manager().to_dict(guid)
        return response.json(result)

    data = request.json
    logger.info("received message: {}".format(obfuscate(data,'message')))

    # session = await LivechatSession.get_session(data['sender'],guid,data["organisation_name"])
    session = SessionManager.get_session_manager().get_session(data['sender'])
    # session = await LivechatSession.get_session(data['sender'],guid,data.get("organisation_name", ORG_NAMES.get(data["sender"]) or ""))

    if data.get('event',None):
        if data['event'] == 'user_disconnected':
            if session: 
                session.cancel() 
    else:
        if not session:
            session = SessionManager.get_session_manager().create_session(guid,data['sender'])
        message = data.get("message", "")
        page_url = data.get("chatStartedOnPage",None)
        starttime = datetime.datetime.now()
        if message.startswith("/init"):
            logger.info("webhook event /init: 0 - {}".format(starttime))
            init_payload = json.loads(data["message"].split("/init")[1])
            history = init_payload["chat_history_plain"]
            page_url = data.get("chatStartedOnPage")
            if await session.init(data["organisation_name"],history,page_url):
                pass
            else:
                await send_router_message(data['sender'],custom={"deescalate": True, "status": "error", "error": "OBI connectie niet gelukt"})
        else:
            #TODO: on init page is in message, on normal message in customData:
            if not page_url:
                customData = data.get('customData',{})
                page_url = customData.get('chatStartedOnPage',None)
            logger.info("page_url: {}, session.current_page: {}".format(page_url,session.current_page))
            if page_url and not session.current_page or session.current_page != page_url:
                session.current_page = page_url
                await session.sendPageMessage(page_url)

            if data.get('customData',{}).get('livechat_queue',False):
                logger.info("webhook event livechatqueue usermessage: 0 - {}".format(starttime))
                await session.sendContextMessage(message)
            else:
                logger.info("webhook event usermessage: 0 - {}".format(starttime))
                await session.sendMessage(message)
    
    return response.text("success")


async def send_router_message(sender_id, text = None, custom = None, sender_name = None, avatar_url = None):
    logger.info(
        "Sending message to router: %s, %s, %s, %s",
        sender_id,
        obfuscate(text),
        custom, 
        sender_name,
    )
    msg = {
        "recipient_id": sender_id,
        "metadata": {
            "avatarUrl": avatar_url,
        },
    }
    if text:
        msg['text'] = text
    if custom:
        msg['custom'] = custom
        # if custom.get("deescalate"): TODO: is this solved elsewhere, why was this here???? Dylan
        #     SESSIONS.pop(sender_id)
    if sender_name:
        msg['metadata']['senderName'] = sender_name
    SessionManager.get_session_manager().push_callback_msg(msg)
    await asyncio.sleep(0)

@app.listener('after_server_start')
async def server_start(app, loop):
    sessions = SessionManager.get_session_manager().get_sessions()
    logger.info("START SERVER FOUND SESSIONS: {}".format(sessions))
    app.add_task(send_callback_messages(),name = "callback") #start coroutine that connects websocket and manages it until daychange (_checkhealth)
    
    for sender_id in sessions:
        logger.info("RECOVERING SESSION: {}".format(sender_id))
        session = SessionManager.get_session_manager().get_session(sender_id)
        await session.up()

@app.listener('before_server_stop')
async def server_stop(app, loop):
    logger.info("STOP SERVER")
    await app.cancel_task("callback")


app.blueprint(cb)


@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    result = {"status": "ok"} | SessionManager.get_session_manager().to_dict()
    return response.json(result)


    

if __name__ == "__main__":
    app.run(host="0.0.0.0")
    configsession.close()
