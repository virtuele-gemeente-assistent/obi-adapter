import datetime
import logging

logger = logging.getLogger(__name__)


def generateTimeStamp():
    datetime_now = datetime.datetime.now()
    datetime_stamp = datetime_now.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    logger.info("OBI4wan: generateTimeStamp: {0}".format(datetime_stamp))
    datetime_stamp += "Z"
    return datetime_stamp


def convertTimeStamp(timestamp):
    datetime_stamp = datetime.datetime.fromtimestamp(timestamp).strftime(
        "%Y-%m-%dT%H:%M:%S.%f"
    )[:-3]
    logger.info("OBI4wan: convertTimeStamp: datetime_stamp: {0}".format(datetime_stamp))
    datetime_stamp += "Z"
    return datetime_stamp
