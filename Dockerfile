# Extend the official Rasa Core SDK image
FROM registry.gitlab.com/virtuele-gemeente-assistent/gem/base-api:latest

WORKDIR /usr/local/src/obi-adapter
COPY ./src /usr/local/src/obi-adapter
ENV PYTHONPATH "${PYTHONPATH}:/usr/local/src/obi-adapter"
COPY ./src/server.py /usr/local/bin/server
RUN chmod +x /usr/local/bin/server
CMD [ "server"]